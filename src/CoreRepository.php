<?php
/**
 * Created by PhpStorm.
 * User: hautruong
 * Date: 02/12/2016
 * Time: 11:52
 */

namespace TCH\CoreRepository;


use Doctrine\ORM\EntityRepository;

abstract class CoreRepository extends EntityRepository implements CoreRepositoryInterface
{
    public $defaultEntityName;

    /**
     * setDefaultEntityName
     * @param string $defaultEntityName
     * @return mixed
     */
    abstract function setDefaultEntityName(string $defaultEntityName);

    /**
     * getById
     * @param $id
     * @param $columns
     * @return mixed
     */
    public function getById($id, $columns = [])
    {
        // TODO: Implement getById() method.
    }

    /**
     * applyWhere
     * @param array $whereConditions
     * @return mixed
     */
    public function applyWhere($whereConditions = [])
    {
        // TODO: Implement applyWhere() method.
    }

    /**
     * addWhereIn
     * @param array $whereInCondition
     * @return mixed
     */
    public function addWhereIn($whereInCondition = [])
    {
        // TODO: Implement addWhereIn() method.
    }

    /**
     * @param array $orderBy
     * @return mixed
     */
    public function applyOrderBy($orderBy = [])
    {
        // TODO: Implement applyOrderBy() method.
    }

    /**
     * @param $entity
     * @param bool $flush
     * @return mixed
     */
    public function save($entity, $flush = false)
    {
        // TODO: Implement save() method.
    }
}