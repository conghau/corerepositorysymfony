<?php

/**
 * Created by PhpStorm.
 * User: hautruong
 * Date: 02/12/2016
 * Time: 11:50
 */

namespace TCH\CoreRepository;

/**
 * Interface CoreRepositoryInterface
 * @package TCH\CoreRepository
 */
interface CoreRepositoryInterface
{
    /**
     * getById
     * @param $id
     * @param $columns
     * @return mixed
     */
    public function getById($id, $columns = []);

    /**
     * applyWhere
     * @param array $whereConditions
     * @return mixed
     */
    public function applyWhere($whereConditions = []);

    /**
     * addWhereIn
     * @param array $whereInCondition
     * @return mixed
     */
    public function addWhereIn($whereInCondition = []);

    /**
     * @param array $orderBy
     * @return mixed
     */
    public function applyOrderBy($orderBy = []);

    /**
     * @param $entity
     * @param bool $flush
     * @return mixed
     */
    public function save($entity, $flush = false);
}